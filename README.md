## Получение индивидуальной выгрузки каталога:
```
$service = new \Portobello\ExternalApi\ProductsXmlService();

$service->loadWithDiscounts(
    'login',
    'password',
    'Путь до файла куда будет сохранен xml',
);
```

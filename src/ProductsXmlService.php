<?php

namespace Portobello\ExternalApi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class ProductsXmlService {

    /**
     * @var Client|null
     */
    private $client = null;

    /**
     * @param string $login
     * @param string $password
     * @param string $filePath
     * @throws AuthException
     * @throws Exception
     */
    public function loadWithDiscounts(string $login, string $password, string $filePath): void {
        $this->auth($login, $password);

        $this->runGeneration();

        while (!$this->generatingIsComplete()) {
            sleep(5);
        }

        $this->saveFile($filePath);
    }

    /**
     * @throws Exception
     */
    private function runGeneration(): void {
        $answer = $this->sendRequestForXml();

        if ($answer->success === false) {
            throw new Exception('Не удалось сгенерировать xml', 502);
        }
    }

    /**
     * @throws Exception
     */
    private function generatingIsComplete(): bool {
        $answer = $this->sendRequestForXml();

        if ($answer->success === false) {
            throw new Exception('Не удалось сгенерировать xml', 502);
        }

        if ($answer->inProcess) {
            return false;
        }

        return $answer->success === true;
    }

    /**
     * @throws Exception
     */
    private function saveFile(string $filePath): void {
        try {
            $response = $this->getClient()->request('GET', '/export/new-products/by-user/download/');
        } catch (GuzzleException $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }

        if ($response->getStatusCode() !== 200) {
            throw new Exception($response->getBody()->getContents(), 502);
        }

        $result = file_put_contents($filePath, $response->getBody()->getContents());

        if ($result === false) {
            throw new Exception('Не удалось сохранить xml', 500);
        }
    }

    /**
     * @return object
     * @throws Exception
     */
    private function sendRequestForXml() {
        try {
            $response = $this->getClient()->request('GET', '/export/new-products/by-user/');
        } catch (GuzzleException $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }

        if ($response->getStatusCode() !== 200) {
            throw new Exception($response->getBody()->getContents(), 502);
        }

        return json_decode($response->getBody()->getContents());
    }

    /**
     * @throws AuthException
     * @throws Exception
     */
    private function auth(string $login, string $password): void {
        try {
            $response = $this->getClient()->request('POST', '/ajax/popups/auth-process/', [
                'form_params' => [
                    'authFormLogin'    => $login,
                    'authFormPassword' => $password,
                ]
            ]);
        } catch (GuzzleException $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }

        if ($response->getStatusCode() !== 200) {
            throw new AuthException($response->getBody()->getContents(), 502);
        }

        $answer = json_decode($response->getBody()->getContents());

        if (!$answer->result) {
            if (!empty($answer->errors)) {
                throw new AuthException(json_encode($answer->errors), 400);
            } elseif ($answer->message) {
                throw new AuthException($answer->message->text, 400);
            } else {
                throw new AuthException('Ошибка', 400);
            }
        }
    }

    /**
     * @return Client
     */
    private function getClient(): Client {
        if ($this->client === null) {
            $this->client = new Client([
                'base_uri' => 'https://portobello.ru',
                'timeout'  => 10,
                'cookies'  => true,
            ]);
        }
        return $this->client;
    }
}
